# Create a file for the IB2 lab
# Author: Simon Vandevelde

with open("grootbestand.txt", "w") as fp:
    fp.write("Ik ben een groot bestand!\n")
    fp.write("Ik kan tellen van 0 tot 1000 -- kijk maar:\n")
    for i in range(0, 1001):
        fp.write(str(i) + "\n")

print("Klaar!")
