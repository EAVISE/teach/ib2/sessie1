# Programma om de reeks van Fibonacci te printen

seq = [0, 1]
i = 0
while i < 10:
    
    new_nr = seq[-1] + seq[-2]
    seq.append(new_nr)
    
    i += 1

for nr in seq:
    print(nr, nr)
